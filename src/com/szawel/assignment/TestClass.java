package com.szawel.assignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TestClass {

	public static void main(String[] args) throws IOException 
	{
		BotExecution execution = new BotExecution();
		execution.execute();
	}	
}

class BotExecution {
	
	public void execute() throws FileNotFoundException{
		FileReader fileFrom = new FileReader("input.txt");
		File fileTo = new File("output.txt");
		
		ArrayList<String> temporaryList = getTemporaryList(fileFrom);
		
		ArrayList<String> outList = new ArrayList<>();	
		
		for(int i = 1; i < temporaryList.size(); i = i+2){
			outList.add(getFinalPositionAndDirection(temporaryList, 
					getMaxX(temporaryList), 
					getMaxY(temporaryList), 
					i));
		}
		
		write(fileTo, outList);
	}
	
	public  Position moveBot(String movements, int initialPositionX, int initialPositionY, char direction, int maxX, int maxY)
	{
		Position position = new Position(initialPositionX, initialPositionY, direction);
		for(int i = 0; i < movements.length(); i++)
		{
			moveOneStep(movements, maxX, maxY, position, i);
		}

		return position;	
	}

	private String getFinalPositionAndDirectionString(Position position) {
		String finalPositionX = Integer.toString(position.getX());
		String finalPositionY = Integer.toString(position.getY());
		String finalDirection = Character.toString(position.getDirection());
		String finalPositionAndDirection = finalPositionX +" "+ finalPositionY + " " + finalDirection;
		return finalPositionAndDirection;
	}

	private void moveOneStep(String movements, int maxX, int maxY,
			Position position, int characterNumber) {
		handleFMovement(movements, position, characterNumber);
		
		handleLMovements(movements, position, characterNumber);
		
		handleRMovement(movements, position, characterNumber);
		
		fixIfLimitsExceeded(maxX, maxY, position);
	}

	private void handleRMovement(String movements, Position position, int characterNumber) {
		if(movements.charAt(characterNumber)=='R')
		{
			moveRight(position);
		}
	}

	private void handleLMovements(String movements, Position position,
			int characterNumber) {
		if(movements.charAt(characterNumber)=='L')
		{
			moveLeft(position);
		}
	}

	private void handleFMovement(String movements, Position position,
			int characterNumber) {
		if(movements.charAt(characterNumber) == 'F')
		{
			moveForward(position);
		}
	}
	
	private void moveRight(Position position) {
		if(isPointingNorth(position)) {
			position.setDirection('E');
		} else if(isPointingWest(position)) {
			position.setDirection('N');
		} else if(isPointingSouth(position)) {
			position.setDirection('W');
		} else if(isPointingEast(position)) {
			position.setDirection('S');
		}
	}
	
	private void moveLeft(Position position) {
		if(isPointingNorth(position)) {
			position.setDirection('W');
		} else if(isPointingWest(position)) {
			position.setDirection('S');
		} else if(isPointingSouth(position)) {
			position.setDirection('E');
		} else if(isPointingEast(position)) {
			position.setDirection('N');
		}
	}

	private void moveForward(Position position) {
		if(isPointingNorth(position)) {
			position.setY(position.getY() + 1);
		} else if(isPointingWest(position)) {
			position.setX(position.getX() - 1);
		} else if(isPointingSouth(position)) {
			position.setY(position.getY() - 1);
		} else if(isPointingEast(position)) {
			position.setX(position.getX() + 1);
		}
	}

	private void fixIfLimitsExceeded(int maxX, int maxY,
			Position position) {
		if(position.getY() > maxY)
		{
			position.setY(maxY);
		} else if(position.getY() < 0) {
			position.setY(0);
		}
		
		if(position.getX() > maxX)
		{
			position.setX(maxX);
		} else if(position.getX() < 0) {
			position.setX(0);
		}
	}
	
	private boolean isPointingNorth(Position position) {
		return position.getDirection() == 'N';
	}

	private boolean isPointingEast(Position position) {
		return position.getDirection() == 'E';
	}

	private boolean isPointingSouth(Position position) {
		return position.getDirection() == 'S';
	}

	private boolean isPointingWest(Position position) {
		return position.getDirection() == 'W';
	}
	
	private void write(File a, ArrayList<String> inList) {
		try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(a))) {

			for (String s : inList) {
				bufferedWriter.write(s + "\r\n");

			}
			
			bufferedWriter.close();
		} catch (IOException ex) {
			System.err.println("Unable to write file " + a + ": " + ex.getMessage());
		}
	}
	
	private ArrayList<String> getTemporaryList(FileReader fileFrom){
		BufferedReader bufferedReader = new BufferedReader(fileFrom);
		
		ArrayList<String> temporaryList = new ArrayList<>();
		
		String line;
		
		try {
			while((line = bufferedReader.readLine()) != null){
				temporaryList.add(line);
			}
			
			bufferedReader.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return temporaryList;
	}
	
	private String getMovements(ArrayList<String> temporaryList, int i) {
		return temporaryList.get(i+1);
	}

	private char getDirection(ArrayList<String> temporaryList, int i) {
		return temporaryList.get(i).charAt(4);
	}

	private int getInitialPositionY(ArrayList<String> temporaryList,
			int i) {
		return Character.getNumericValue(temporaryList.get(i).charAt(2));
	}

	private int getInitialPositionX(ArrayList<String> temporaryList,
			int i) {
		return Character.getNumericValue(temporaryList.get(i).charAt(0));
	}

	private int getMaxY(ArrayList<String> temporaryList) {
		return Character.getNumericValue(temporaryList.get(0).charAt(2));
	}

	private int getMaxX(ArrayList<String> temporaryList) {
		return Character.getNumericValue(temporaryList.get(0).charAt(0));
	}
	
	private String getFinalPositionAndDirection(
			ArrayList<String> temporaryList, int maxX, int maxY, int i) {
		int initialPositionX = getInitialPositionX(temporaryList, i);
		int initialPositionY = getInitialPositionY(temporaryList, i);
		
		char direction = getDirection(temporaryList, i);
		String movements = getMovements(temporaryList, i);
		
		Position finalPosition = moveBot(movements, initialPositionX, initialPositionY, direction, maxX , maxY);
		return getFinalPositionAndDirectionString(finalPosition);
	}
}

class Position {
	private int x, y;
	private char direction;
	
	public Position(int x, int y, char direction){
		this.x = x;
		this.y = y;
		this.direction = direction;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public void setDirection(char direction){
		this.direction = direction;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
	public char getDirection(){
		return direction;
	}
}
